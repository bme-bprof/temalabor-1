Témalabor 1 Short worklog
=========================

Dátum         | Idő              | Tevékenység                                    | 
-------------:| :--------------: | ---------------------------------------------- |
2021-02-22    | 12:00 - 12:45    | Adatkeresés                                    |
2021-02-22    | 13:55 - 14:05    | Adatvizualizációs eszköz kiválasztása          |
2021-02-23    | 14:00 - 17:00    | python kód megírása és SODA API engine tanulás |
2021-02-25    | 11:23 - 15:05    | Adatvizualizáció áttekintése és fixálása       |
2021-02-26    | 14:08 - 15:10    | Videó felvétele és szerkesztése                |