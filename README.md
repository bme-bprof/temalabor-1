# 2021 tavaszi Témalaboratórium első project *(DP1EVM - Péteri Ádám Gábor)*  #

BME adatalapú specailizáció témalaboratóriumának első feladata.

## Project rövid leírása ###

A project [Austin város publikus adatbázisára](https://data.austintexas.gov/) épülő adatvizualizációs python script

## Adatok amiket feldolgozok 

Az adatok egy [SODA API engine](https://dev.socrata.com/) hívásával kéri ki a JSON adatokat.


*Adatfolyam egy szegmense:*

```json
{
	"go_primary_key":201610188,
	"council_district":8,
	"go_highest_offense_desc":"AGG ASLT ENHANC STRANGL/SUFFOC",
	"highest_nibrs_ucr_offense_description":"Agg Assault",
	"go_report_date":"1-Jan-16",
	"go_locatio":"8600 W SH 71",
	"clearance_status":"C",
	"clearance_date":"12-Jan-16",
	"go_district":"D",
	"go_location_zip":78735,
	"go_census_tract":19.08,
	"go_x_coordinate":3067322,
	"go_y_coordinate":10062796
}
```

> Egyes szegmenesek **nem tartalmazzák ugyan azokat a fildeket** ennek oka az adatok röggzités az USA-ban.

A JSON adatokat python [Bokeh](https://docs.bokeh.org/en/latest/index.html) librari vizualizálja.

## Kimeneti adatok ##

A kimenete a scriptnek különféle diagrammok és képek lesznek ahol például 2020-as évben elkövetett New Yorkban történt lövöldözések adatit vizualizálja térképen és különféle statisztikáka mutat be róla

# [Videó a projectről](https://drive.google.com/file/d/1r17HzrxqfPcOMIi3Q0klnZxetdEAvxiR/view?usp=sharing) #


Témalabor 1 Short worklog
=========================


Dátum		| Idő			| Tevékenység										| 

2021-02-22	| 12:00 - 12:45	| Adatkeresés										|

2021-02-22	| 13:55 - 14:05	| Adatvizualizációs eszköz kiválasztása				|

2021-02-23	| 14:00 - 17:00	| python kód megírása és SODA API engine tanulás	|

2021-02-25	| 11:23 - 15:05	| Adatvizualizáció áttekintése és fixálása			|

2021-02-26	| 14:08 - 15:10	| Videó felvétele és szerkesztése					|